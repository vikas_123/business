<?php
 abstract class My_Controller_Action_Abstract extends Zend_Controller_Action {

    public $siteContent = null;
    protected $siteNotification;
    protected $auth_user;
    protected $category;

    protected $_flashMessenger;
	
	public function preDispatch() {
		parent::preDispatch();
		$this->setBaseUrl();
                
        $auth = Zend_Auth::getInstance();
		if (isset($_POST['PHPSESSID'])) Zend_Session::setId($_POST['PHPSESSID']);
        
        $requestAction = $this->getRequest()->getParam('controller');
        $userControllerArray = array('shopping-cart','profile','checkout');
        $adminControllerArray = array('admin');
        $request = $this->getRequest();
//        dd($auth->getIdentity());
       $adminControllerArray = array('admin');
       $request = $this->getRequest();
      // dd($auth->getIdentity());
       if(in_array($requestAction,$adminControllerArray)) {

        if(!$auth->getIdentity() ){

          $this->_helper->flashMessenger->addMessage('Login Session Timeout');
    
           $this->_redirect($this->getBaseURL().'/auth/login');
               
            }
            else {
            $data = $auth->getIdentity();
            if($data['type']== 'admin') {
                $this->view->admin_user = $this->admin_user = $auth->getIdentity();
                }
                else {
                 $this->_redirect($this->getBaseURL().'/auth/login');
                }
                // DD('admin_user');
            }
       }
             
	       $this->category = new Default_Model_Category();
         $data=$this->category->getRowByFilters();
         // dd($data);
         $this->view->menubar=$data;



		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$this->view->flashMessages = implode('. ', $this->_flashMessenger->getMessages());

		//Setting the request for view 
		$this->view->request = new My_Request();
		$this->view->baseUrl = $this->baseUrl = Zend_Controller_Front::getInstance()->getBaseURL();
		$auth = Zend_Auth::getInstance();
		
		//Setting the Paging File 
		Zend_View_Helper_PaginationControl::setDefaultViewPartial('pagination.html');
		$this->view->addHelperPath(APPLICATION_PATH . '/default/views/helpers', 'Default_View_Helper');
	}
	
	public function makeUrl($url, $module = '') {
		if (!$module) {
			$module = Zend_Controller_Front::getInstance()->getRequest()->getParam('module');
		}
		if($module != 'default') {
			$url =  '/' . $module . $url;
		}
		$settingConfig = new Zend_Config_Ini(APP_DIR . DS . 'configs' . DS . 'setting.ini');
		if($settingConfig->base_folder) {
			$url = '/' . $settingConfig->base_folder . $url;
		}
		$host  = $_SERVER['HTTP_HOST'];
		$proto = (empty($_SERVER['HTTPS'])) ? 'http' : 'https';
		
		return $proto . '://' . $host.$url;
	}
	
	protected function flash($message, $redirect=false) {
		$messageText = $this->siteNotification->getMessage($message);
		if (!$messageText) {
			$messageText = $message;
		}
		
		$this->_flashMessenger->addMessage($messageText);
		
		if ($redirect) {
			if (is_array($redirect)) {
				$this->_helper->redirector($redirect[0], isset($redirect[1]) ? $redirect[1] : null);
			} else {
				$this->_redirect($redirect);
			}
		}
	}
	
	protected function sendEmail($to, $code, $params, $attachment=null) {
		$email = new My_Mail($to);
		$subject = htmlspecialchars_decode($this->siteNotification->getTitle($code, $params));
		$message = $this->siteNotification->getMessage($code, $params);
        $email->send($subject, $message, $attachment);
	}

	protected function getBaseURL() {
		$host  = $_SERVER['HTTP_HOST'];
                $settingConfig = new Zend_Config_Ini(APP_DIR . DS . 'configs' . DS . 'setting.ini');
                if ($settingConfig->base_folder) {
                    $host .= '/' . $settingConfig->base_folder;
                }
		$proto = (empty($_SERVER['HTTPS'])) ? 'http' : 'https';
		return $proto . '://' . $host;
	}
	
	protected function setBaseUrl($url = '', $addModuleName = null) {
		if ($addModuleName) $url = '/' . $addModuleName . $url;
		$settingConfig = new Zend_Config_Ini(APP_DIR . DS . 'configs' . DS . 'setting.ini');
		if($settingConfig->base_folder) $url = '/' . $settingConfig->base_folder . $url;
		Zend_Controller_Front::getInstance()->setBaseUrl($url);
	}

	protected function _getCredenital() {
		if (isset($_SERVER['HTTP_X_FORWARD_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARD_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		return md5($ip);//$_SERVER['HTTP_USER_AGENT'] .
	}
	
	protected function _getIpaddress() {
		if (isset($_SERVER['HTTP_X_FORWARD_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARD_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		return $ip;//$_SERVER['HTTP_USER_AGENT'] .
	}

	protected function _generateCaptcha($name, $config = array())
	{

	}
	public function postDispatch() {
    }
}
