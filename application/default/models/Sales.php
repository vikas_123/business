<?php

class Default_Model_Sales extends My_Db_Table_Abstract {

    protected $_name = 'sales';
    protected $_primary = 'id';

    public function getRowByFilters($filters = array(), $row = FALSE) {

        $db = Zend_Registry::get('db');

        $select = $db->select()->from('sales');
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }

        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }

    public function getSalesDetails($filters = array(), $row = FALSE) {

        $db = Zend_Registry::get('db');

        $select = $db->select()->from('sales');
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }

        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }

    public function setSalesDetails($dataSet = array())
    {
        $db = Zend_Registry::get('db');
        $db->insert('sales', $dataSet);
    //    dd($dataSet);
        $id = $db->lastInsertId('sales', 'id');
        return $id;
    }

    public function setSalesProductDetails($dataSet = array())
    {
         $db = Zend_Registry::get('db');
        $db->insert('sales_product', $dataSet);
    //    dd($dataSet);
        $id = $db->lastInsertId('sales_product', 'id');
        return $id;
    }

    public function getSalesProductDetails($filters = array(), $row = FALSE)
    {
        $db = Zend_Registry::get('db');

        $select = $db->select()->from('sales_product')
        ->joinLeft('product_details','product_details.product_id = sales_product.product_id ',array('product_name','color','unit_price'))
        ->joinLeft('product_images','product_images.product_id = sales_product.product_id and is_primary=1',array('image'));
//        die($select);
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }

        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
//        die($select);
    }


    //function to set deleted sales
    public function deleteSalesDetails($id = null) {

        $db = Zend_Registry::get('db');
        $dataSet = array(
            'deleted_at' => time());
        $response = $db->update('sales', $dataSet, 'id =' . $id);
        return $response;
    }

    //update user 
    public function updateSalesById($dataSet = null, $id = null) {
        $db = Zend_Registry::get('db');
        $response = $db->update('sales', $dataSet, 'id =' . $id);
        return $response;
    }

}
