<?php

class Default_Model_Details extends My_Db_Table_Abstract {

    protected $_name = 'details';
    protected $_primary = 'id';
    protected $_rowClass = 'Default_Model_DetailsRow';
    protected $_dependentTables = array();
    protected $_referenceMap = array();
    protected $_filters = array();
    protected $_validators = array();

    public function getRowByFilters($filters = array(), $row = FALSE) {

        $db = Zend_Registry::get('db');

        $select = $db->select()->from('details');
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }

        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }

    public function setUserDetails($dataSet = array()) {
        $db = Zend_Registry::get('db');

        $db->insert('details', $dataSet);
        $id = $db->lastInsertId('details', 'id');
        return $id;
    }

    //function to remove user form the db
    public function deleteUserDetails($userId = null) {

        $db = Zend_Registry::get('db');
        $response = $db->delete('details', 'details.user_id =' . $userId);
        return $response;
    }

    //update user 
    public function updateUserByUserId($dataSet = null, $userId = null) {
        $db = Zend_Registry::get('db');
        $response = $db->update('details', $dataSet, 'id =' . $userId);
        return $response;
    }

}
