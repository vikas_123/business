<?php

class Default_Model_Products extends My_Db_Table_Abstract {
    
    protected $_name = 'products';
    protected $_primary = 'id';
    protected $_rowClass = 'Default_Model_ProductsRow';
    protected $_dependentTables = array();
    protected $_referenceMap = array();
    protected $_filters = array();
    protected $_validators = array();

    public function getRowByFilters($filters = array(), $row = FALSE, $orderType = NULL) {

        $db = Zend_Registry::get('db');

        $select = $db->select()->from('products')
                ->joinLeft('product_details','product_details.product_id = products.id')
                ->joinLeft('brands','brands.id = product_details.brand_id',array('brand_name'))
                ->joinLeft('product_images','product_images.product_id = products.id and is_primary=1',array('image'))
                ->joinLeft('category','category.id = product_details.category_id',array('category_name'));
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }
        
       
        $select->group('products.id');
        if($orderType) {
            $select->order('products.id '.$orderType);
        }

        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }
    
     public function searchProducts($filters = null, $row = FALSE) {

        $db = Zend_Registry::get('db');

        $select = $db->select()->from('products')
                ->joinLeft('product_details','product_details.product_id = products.id')
                ->joinLeft('brands','brands.id = product_details.brand_id',array('brand_name'))
                ->joinLeft('product_images','product_images.product_id = products.id',array('image'))
                ->joinLeft('category','category.id = product_details.category_id',array('category_name'));
        if ($filters) {
                $select->where('product_details.product_name LIKE ? ', '%'.$filters.'%')
                        ->orWhere('product_details.features LIKE ? ', '%'.$filters.'%')
                        ->orWhere('product_details.tags LIKE ? ', '%'.$filters.'%')
                        ->orWhere('product_details.product_name LIKE ? ', '%'.$filters.'%')
                        ->orWhere('brands.brand_name LIKE ? ', '%'.$filters.'%')
                        ->orWhere('category.category_name LIKE ? ', '%'.$filters.'%')
                        ->orWhere('products.sku LIKE ? ', '%'.$filters.'%')
                        ;
        }
        
       
        $select->group('products.id');
//        die($select);
        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }
    //get product images
     public function getProductImages($filters = array(), $row = FALSE) {

        $db = Zend_Registry::get('db');

        $select = $db->select()->from('product_images');
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }

        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }
    
    //add new project
    public function setProduct($dataSet = array())
    {
        $db = Zend_Registry::get('db');

        // dd($dataSet);
        $db->insert('products', $dataSet);
        //dd($dataSet);
        $id = $db->lastInsertId('products', 'id');
        return $id;
    }
    
    //add project details
    public function setProductsDetails($dataSet = array())
    {
        $db = Zend_Registry::get('db');

        // dd($dataSet);
        $db->insert('product_details', $dataSet);
        //dd($dataSet);
        $id = $db->lastInsertId('product_details', 'id');
        return $id;
    }

     //function to remove user form the db
    public function addProductImage($dataSet = array()) {
        $db = Zend_Registry::get('db');
         $db->insert('product_images', $dataSet);
//        dd($dataSet);
        $id = $db->lastInsertId('product_images', 'id');
        return $id;
    }
    
    //function to remove user form the db
    public function deleteProductsDetails($id = null) {
        $db = Zend_Registry::get('db');
        $response = $db->delete('products', 'products.id =' . $id);
        return $response;
    }
//update product images
    public function updateProductImages($dataSet = null, $field = null, $fValue = null) {
        $db = Zend_Registry::get('db');
        $response = $db->update('product_images', $dataSet, $field.' =' . $fValue);// set 'is_primary'=> 0 where $field= $fvalue
        return $response;
    }
    //delete product images
    public function deleteProductImage($id=null)
    {
        $db=  Zend_Registry::get('db');
        $response=$db->delete('product_images','product_images.id='.$id);
        return $response;
    }
    //update product details 
    public function updateProductsDetailsById($dataSet = null, $id = null) {
        $db = Zend_Registry::get('db');
        $response = $db->update('product_details', $dataSet, 'product_id =' . $id);
        return $response;
    }
    //update product 
    public function updateProductsById($dataSet = null, $id = null) {
        $db = Zend_Registry::get('db');
        $response = $db->update('products', $dataSet, 'id =' . $id);
        return $response;
    }

}

?>
