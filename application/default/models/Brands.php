<?php

class Default_Model_Brands extends My_Db_Table_Abstract {

    protected $_name = 'brands';
    protected $_primary = 'id';

    public function getRowByFilters($filters = array(), $row = FALSE) {

        $db = Zend_Registry::get('db');

        $select = $db->select()->from('brands');
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }

        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }

    public function setBrandsDetails($dataSet = array())
    {
        $db = Zend_Registry::get('db');
        $db->insert('brands', $dataSet);
    //    dd($dataSet);
        $id = $db->lastInsertId('brands', 'id');
        return $id;
    }

    //function to remove user form the db
    public function deleteBrandsDetails($id = null) {

        $db = Zend_Registry::get('db');
        $response = $db->delete('brands', 'brands.id =' . $id);
        return $response;
    }

    //update user 
    public function updateBrandsById($dataSet = null, $id = null) {
        $db = Zend_Registry::get('db');
        $response = $db->update('brands', $dataSet, 'id =' . $id);
        return $response;
    }

}
