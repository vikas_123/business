<?php

class Default_Model_SalesProduct extends My_Db_Table_Abstract {

    protected $_name = 'sales_product';
    protected $_primary = 'id';

    public function getRowByFilters($filters = array(), $row = FALSE) {

        $db = Zend_Registry::get('db');

        $select = $db->select()->from('sales_product');
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }

        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }

    public function setSalesProductDetails($dataSet = array())
    {
        $db = Zend_Registry::get('db');
        $db->insert('sales_product', $dataSet);
    //    dd($dataSet);
        $id = $db->lastInsertId('sales_product', 'id');
        return $id;
    }
    // die($dataSet);

    //function to set deleted sales_product
    public function deleteSalesProductDetails($id = null) {

        $db = Zend_Registry::get('db');
        $dataSet = array(
            'deleted_at' => time());
        $response = $db->update('sales_product', $dataSet, 'id =' . $id);
        return $response;
    }

    //update user 
    public function updateSalesProductById($dataSet = null, $id = null) {
        $db = Zend_Registry::get('db');
        $response = $db->update('sales_product', $dataSet, 'id =' . $id);
        return $response;
    }

}
