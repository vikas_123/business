<?php

class Default_Model_MinisterSelect extends My_Db_Table_Abstract {

    protected $_name = 'minister_select';
    protected $_primary = 'user_id';
    protected $_rowClass = 'Default_Model_MinisterSelectRow';
    protected $_dependentTables = array();
    protected $_referenceMap = array();
    protected $_filters = array();
    protected $_validators = array();

    public function getRowByFilters($filters = array(), $row = FALSE) {
      
        $db = Zend_Registry::get('db');
        $select = $db->select()->from('minister_select');
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }

        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }

   

    public function setUser($dataSet = array()) {
        $db = Zend_Registry::get('db');
         // dd($dataSet);
        $respose=$db->insert('minister_select', $dataSet);
       // die($respose);
        $id = $db->lastInsertId('minister_select', 'id');
        return $id;
    }

    //function to remove user form the db
    public function deleteUser($userId = null) {

        $db = Zend_Registry::get('db');
        $response = $db->delete('minister_select', 'minister_select.user_id =' . $userId);
        return $response;
    }

    //update user 
    public function updateUserByUserId($dataSet = null, $userId = null) {
        $db = Zend_Registry::get('db');
        $response = $db->update('minister_select', $dataSet, 'user_id =' . $userId);
        return $response;
    }

}
