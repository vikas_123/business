<?php

class Default_Model_Portfolio extends My_Db_Table_Abstract {

    protected $_name = 'portfolio';
    protected $_primary = 'user_id';
    protected $_rowClass = 'Default_Model_portfolioRow';
    protected $_dependentTables = array();
    protected $_referenceMap = array();
    protected $_filters = array();
    protected $_validators = array();

    public function getRowByFilters($filters = array(), $row = FALSE) {
      
      $d=0;
        $db = Zend_Registry::get('db');
        $select = $db->select()->from('portfolio');
        
        // die($select);
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }

        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }

     

    public function setUser($dataSet = array()) {
        $db = Zend_Registry::get('db');
        
        $respose=$db->insert('portfolio', $dataSet);
        $id = $db->lastInsertId('portfolio', 'id');

        return $id;
    }
     

    //function to remove user form the db
    public function deleteUser($userId = null) {

        $db = Zend_Registry::get('db');
        $response = $db->delete('portfolio', 'portfolio.id =' . $userId);
        return $response;
    }

    //update user 
    public function updateUserByUserId($dataSet = null, $userId = null) {
        // dd('update');
        $db = Zend_Registry::get('db');
        $response = $db->update('portfolio', $dataSet, 'id =' . $userId);
        return $response;
    }

}
