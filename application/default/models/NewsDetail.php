<?php

class Default_Model_NewsDetail extends My_Db_Table_Abstract {

    protected $_name = 'news_detail';
    protected $_primary = 'user_id';
    protected $_rowClass = 'Default_Model_news_detailRow';
    protected $_dependentTables = array();
    protected $_referenceMap = array();
    protected $_filters = array();
    protected $_validators = array();

    public function getRowByFilters($filters = array(), $row = FALSE) {
      
      $d=0;
        $db = Zend_Registry::get('db');
        $select = $db->select()->from('news_detail')
        ->where('deleted_at= ?',$d);
        // die($select);
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }

        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }

    public function setUser($dataSet = array()) {
        $db = Zend_Registry::get('db');
        
        $respose=$db->insert('news_detail', $dataSet);
//        dd($respose);
        $id = $db->lastInsertId('news_detail', 'id');
        return $id;
    }

    //function to remove user form the db
    public function deleteUser($userId = null) {

        $db = Zend_Registry::get('db');
        $response = $db->delete('news_detail', 'news_detail.id =' . $userId);
        return $response;
    }

    //update user 
    public function updateUserByUserId($dataSet = null, $userId = null) {
        // dd('update');
        $db = Zend_Registry::get('db');
        $response = $db->update('news_detail', $dataSet, 'id =' . $userId);
        return $response;
    }

}
