<?php

class Default_Model_Users extends My_Db_Table_Abstract {

    protected $_name = 'users';
    protected $_primary = 'user_id';
    protected $_rowClass = 'Default_Model_UsersRow';
    protected $_dependentTables = array();
    protected $_referenceMap = array();
    protected $_filters = array();
    protected $_validators = array();

    public function getRowByFilters($filters = array(), $row = FALSE) {
      
        $db = Zend_Registry::get('db');
        $select = $db->select()->from('users');
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }

        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }

    public function setUser($dataSet = array()) {
        $db = Zend_Registry::get('db');
        
        $respose=$db->insert('users', $dataSet);
//        dd($respose);
        $id = $db->lastInsertId('users', 'user_id');
        return $id;
    }

    //function to remove user form the db
    public function deleteUser($userId = null) {

        $db = Zend_Registry::get('db');
        $response = $db->delete('users', 'users.user_id =' . $userId);
        return $response;
    }

    //update user 
    public function updateUserByUserId($dataSet = null, $userId = null) {
        $db = Zend_Registry::get('db');
        $response = $db->update('users', $dataSet, 'user_id =' . $userId);
        return $response;
    }

}
