<?php

class Default_Model_OfficerCategory extends My_Db_Table_Abstract {

    protected $_name = 'officer_category';
    protected $_primary = 'user_id';
    protected $_rowClass = 'Default_Model_OfficerCategoryRow';
    protected $_dependentTables = array();
    protected $_referenceMap = array();
    protected $_filters = array();
    protected $_validators = array();

    public function getRowByFilters($filters = array(), $row = FALSE) {
      // dd('hlo');
        $db = Zend_Registry::get('db');
        $select = $db->select()->from('officer_category');
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }

        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }
    

    public function setUser($dataSet = array()) {
        $db = Zend_Registry::get('db');
        
        $respose=$db->insert('officer_category', $dataSet);
//        dd($respose);
        $id = $db->lastInsertId('officer_category', 'id');
        return $id;
    }

    //function to remove user form the db
    public function deleteUser($userId = null) {
        // dd($userId );
        $db = Zend_Registry::get('db');
        $response = $db->delete('officer_category', 'officer_category.id =' . $userId);
        // dd($response);
        return $response;
    }

    //update user 
    public function updateUserByUserId($dataSet = null, $userId = null) {
        $db = Zend_Registry::get('db');
        $response = $db->update('officer_category', $dataSet, 'user_id =' . $userId);
        return $response;
    }

}
