<?php

class Default_Model_MainMinister extends My_Db_Table_Abstract {

    protected $_name = 'mainminister';
    protected $_primary = 'user_id';
    protected $_rowClass = 'Default_Model_mainministerRow';
    protected $_dependentTables = array();
    protected $_referenceMap = array();
    protected $_filters = array();
    protected $_validators = array();

    public function getRowByFilters($filters = array(), $row = FALSE) {
      // dd('hlo');
         $d=0;
        $db = Zend_Registry::get('db');
        $select = $db->select()->from('mainminister')
          ->where('deleted_at= ?',$d);
          
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }
  // die($select);
        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }
    public function getRowByFiltersminister($filters = array(), $row = FALSE) {
      // dd($filters);
         $d=0;
        $db = Zend_Registry::get('db');
        $select = $db->select()->from('mainminister')
          ->where('deleted_at= ?',$d);
          
         
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }
         // die($select);
        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }
     public function getRowByFiltersindex($filters = array(), $row = FALSE) {
      // dd('hlo');
      $d=0;
        $db = Zend_Registry::get('db');
        $select = $db->select()->from('mainminister')
        ->where('deleted_at= ?',$d)
        ->where('user_id=?',$filters['user_id']);
        // die($select);
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }

        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }
  

    public function setUser($dataSet = array()) {
        $db = Zend_Registry::get('db');
        
        $respose=$db->insert('mainminister', $dataSet);
//        dd($respose);
        $id = $db->lastInsertId('mainminister', 'id');
        return $id;
    }

    //function to remove user form the db
    public function deleteUser($userId = null) {
        // dd($userId );
        $db = Zend_Registry::get('db');
        $response = $db->delete('mainminister', 'mainminister.id =' . $userId);
        // dd($response);
        return $response;
    }

    //update user 
    public function updateUserByUserId($dataSet = null, $userId = null) {
        $db = Zend_Registry::get('db');
        $response = $db->update('mainminister', $dataSet, 'id =' . $userId);
        return $response;
    }

}
