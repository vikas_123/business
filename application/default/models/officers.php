<?php

class Default_Model_officers extends My_Db_Table_Abstract {

    protected $_name = 'officers';
    protected $_primary = 'user_id';
    protected $_rowClass = 'Default_Model_officersRow';
    protected $_dependentTables = array();
    protected $_referenceMap = array();
    protected $_filters = array();
    protected $_validators = array();

    public function getRowByFilters($filters = array(), $row = FALSE) {
      
      $d=0;
        $db = Zend_Registry::get('db');
        $select = $db->select()->from('officers')
        ->where('deleted_at= ?',$d);
        // die($select);
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }

        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }

      public function getRowByFiltersindex($filters = array(), $row = FALSE) {
      // dd($filters);
      $d=0;
        $db = Zend_Registry::get('db');
        $select = $db->select()->from('officers')
        ->where('deleted_at= ?',$d)
        ->where('parent_id=?',$filters['parent_id']);
        // die($select);
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }

        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }
     public function getRowByFilters1($filters = array(), $row = FALSE) {

        $db = Zend_Registry::get('db');
        // dd($filters);
        $select = $db->select()->from('officers');
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }
        // die($select);
        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }


    public function setUser($dataSet = array()) {
        $db = Zend_Registry::get('db');
        
        $respose=$db->insert('officers', $dataSet);
        $id = $db->lastInsertId('officers', 'id');

        return $id;
    }
     public function selectMinister($filters = array(), $row = FALSE) {
         

        $db = Zend_Registry::get('db');
        // dd($filters);
        if($filters)
        {
            $id=$filters['parent_id'];

         $select = $db->query("SELECT * FROM officers WHERE id not IN(SELECT id FROM minister_select) and parent_id= $id");
             return $select->fetchAll();
        }
                 
    }

    //function to remove user form the db
    public function deleteUser($userId = null) {

        $db = Zend_Registry::get('db');
        $response = $db->delete('officers', 'officers.id =' . $userId);
        return $response;
    }

    //update user 
    public function updateUserByUserId($dataSet = null, $userId = null) {
        // dd('update');
        $db = Zend_Registry::get('db');
        $response = $db->update('officers', $dataSet, 'id =' . $userId);
        return $response;
    }

}
