<?php

class Default_Model_UserCart extends My_Db_Table_Abstract {

    protected $_name = 'details';
    protected $_primary = 'id';
    protected $_rowClass = 'Default_Model_UserCartRow';
    protected $_dependentTables = array();
    protected $_referenceMap = array();
    protected $_filters = array();
    protected $_validators = array();

    public function getRowByFilters($filters = array(), $row = FALSE) {

        $db = Zend_Registry::get('db');

        $select = $db->select()->from('user_cart');
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }
        $select->where('is_active = ? ', 1);
        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }

    public function getCartDataByFilters($filters = array(), $row = FALSE) {

        $db = Zend_Registry::get('db');

        $select = $db->select()->from('user_cart',array('id as cart_id', 'quantity'))
                   ->joinLeft('product_details','product_details.product_id = user_cart.product_id')
                    ->joinLeft('product_images',' product_images.product_id = user_cart.product_id and is_primary=1 ' );
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }
        $select->where('user_cart.is_active = ? ', 1);
//        die($select);
        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }

    public function addUserCart($dataSet = array()) {
        $db = Zend_Registry::get('db');

        $db->insert('user_cart', $dataSet);
        $id = $db->lastInsertId('user_cart', 'id');
        return $id;
    }

    //function to remove user form the db
    public function deleteUserCart($id = null) {

        $db = Zend_Registry::get('db');
        $response = $db->delete('user_cart', 'user_cart.id =' . $id);
        return $response;
    }

    //update user 
    public function updateUserCartById($dataSet = null, $id = null) {
        $db = Zend_Registry::get('db');
        $response = $db->update('user_cart', $dataSet, 'id =' . $id);
        return $response;
    }

}
