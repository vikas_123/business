<?php

class Default_Model_Category extends My_Db_Table_Abstract {

    protected $_name = 'category';
    protected $_primary = 'id';
    protected $_rowClass = 'Default_Model_CategoryRow';
    protected $_dependentTables = array();
    protected $_referenceMap = array();
    protected $_filters = array();
    protected $_validators = array();

    public function getRowByFilters($filters = array(), $row = FALSE) {
        // dd($filters);
        $db = Zend_Registry::get('db');

        $select = $db->select()->from('category');
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }

        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }


    public function getRowByFilterNews($filters = array(), $row = FALSE) {

        $db = Zend_Registry::get('db');
        $d=0;
        $select = $db->select()->from('category')
        ->where('category_type= ?','news')
        ->where('deleted_at= ?',$d);
        // die($select);
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }

        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }
      public function ministerData($filters = array(), $row = FALSE) {

        $db = Zend_Registry::get('db');
        // dd($filters);
        $select = $db->select()->from('category')
                   ->joinLeft('mainminister','mainminister.user_id = category.id',array('mainminister.*'));
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }

        // die($select);

        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
}
    public function getRowByFiltersName($filters = array(), $row = FALSE) {

        $db = Zend_Registry::get('db');

        $select = $db->select()->from('category',  array('category_name' ));
        if ($filters) {
            foreach ($filters as $key => $value) {
                $select->where($key . ' = ? ', $value);
            }
        }

        if ($row) {
            return $db->fetchRow($select);
        } else {
            return $db->fetchAll($select);
        }
    }

    public function setUser($dataSet = array())
    {
        $db = Zend_Registry::get('db');
//        echo '<pre/>';
//        print_r($dataSet);
//        exit;
        $db->insert('category', $dataSet);
    //    dd($dataSet);
        $id = $db->lastInsertId('category', 'id');
        return $id;
    }

    //function to remove user form the db
    public function deleteCategoryDetails($userId = null) {

        $db = Zend_Registry::get('db');
        $response = $db->delete('category', 'category.user_id =' . $userId);
        return $response;
    }

    //update user 
    public function updateCategoryByUserId($dataSet = null, $userId = null) {
        $db = Zend_Registry::get('db');
        $response = $db->update('category', $dataSet, 'id =' . $userId);
        return $response;

    
    }

}
