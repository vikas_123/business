<?php
//namespace Knp\Snappy;

class IndexController extends My_Controller_Action_Abstract {

protected $users;
protected $category;
protected $portfolio;
protected $contact;
    
   
    public function init() {
     
        $this->users = new Default_Model_Users();
        $this->category = new Default_Model_Category();
        $this->portfolio= new Default_Model_Portfolio();
        $this->contact= new Default_Model_Contact();
       
       
    }

    
    public function indexAction() {
      
      $request=$this->getRequest();

      $data=$this->category->getRowByFilters();
      $this->view->menubar=$data;
      

      $data1=$this->portfolio->getRowByFilters();
          // dd($data1);
          $this->view->dataSet1=$data1;



     
    }

     public function aboutUsAction() {

    
     
    }

     public function blogItemAction() {

    
     
    }
     public function blogAction() {

    
     
    }
     public function contactUsAction() {
      

       $request = $this->getRequest();

       if($request->getPost('name','')){
        // dd('he');

        $request= array(
          'name' => $request->getPost('name',''),
          'phone' => $request->getPost('phone',''),
          'email' => $request->getPost('email',''),
          'subject' => $request->getPost('subject',''),
          'message' => $request->getPost('message',''),
          'price' => $request->getPost('price',''),
          'created_at'=>time()
         );

        // dd($request);
        $res=$this->contact->setUser($request);
        // dd($res);
        $this->view->response=$res;



       }

    
     
    }
     public function portfolioAction() {
         $request = $this->getRequest();


          $data1=$this->portfolio->getRowByFilters();
          $this->view->dataSet1=$data1;
          $data2=$this->portfolio->getRowByFilters( array('type' => 'creative', ));
          $this->view->dataSet2=$data2;
          $data3=$this->portfolio->getRowByFilters( array('type' => 'photography', ));
          $this->view->dataSet3=$data3;
          $data4=$this->portfolio->getRowByFilters( array('type' => 'webdevelopment', ));
          // dd($data3);
          $this->view->dataSet4=$data4;
    }

    public function portfolioDetailAction() {
        // dd('here');
        $request = $this->getRequest();
        $id=$_GET['id'];
        $data=$this->portfolio->getRowByFilters(array('id'=>$id));
        // dd($data);
        $this->view->dataSet=$data;

    
     
    }
     public function pricingAction() {

    
     
    }

     public function servicesAction() {

    
     
    }
     public function shortcodesAction() {

    
     
    }

    public function androidAction() {

    
     
    }

    public function iphoneAction() {

    
     
    }

     public function mobileAppsAction() {

    
     
    }
    public function softwareAction() {

    
     
    }
    public function websiteAction() {

    
     
    }
    public function ecommerceAction() {

    
     
    }
     public function databaseAction() {

    
     
    }
    public function webTechAction() {

    
     
    }
   }