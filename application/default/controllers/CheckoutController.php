<?php
//namespace Knp\Snappy;

class CheckoutController extends My_Controller_Action_Abstract {
   protected $address;
    protected $userCart;
    protected $sales;
    protected $salesProduct;


    public function init() {
       $this->address = new Default_Model_Address();
        $this->userCart = new Default_Model_UserCart();
        $this->sales = new Default_Model_Sales();
        $this->salesProduct = new Default_Model_SalesProduct();
    }

    public function indexAction() {
        
        $user_id = $this->auth_user['user_id'];
        $dataSet['addressList'] = $this->address->getRowByFilters(array('user_id' => $user_id));
        $this->view->dataSet = $dataSet;
       // dd($dataSet);
        
    }
    public function listAction() {
        
 
    }
    public function addAddressAction(){
        
    }
    public function paymentAction(){
           
         $userId = $this->auth_user['user_id'];
        $request= $this->getRequest();
        $type=$request->getPost('payment',''); // gets the payment type
        $address_id = $request->getParam('add_id','');
         // gets the address id
        $address=$request->getPost('address',''); // gets the address id hidded type

        $dataSet = $this->userCart->getCartDataByFilters(array('user_cart.user_id' => $userId));
        // dd($dataSet);
        
        $MERCHANT_KEY = "XA6pdScr";
        $SALT = "Lw07w9poeI";

        //$PAYU_BASE_URL =  "https://secure.payu.in"; //for LIVE mode
        $PAYU_BASE_URL = "https://test.payu.in";
        $action = '';
        $formError = 0;
          // Generate random transaction id 
          $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        $posted = array
         (
            'key' => $MERCHANT_KEY,
            'txnid' => $txnid,
            'amount' => 1500,
            'firstname' => 'Ashish',
            'email' => 'ashish@skydevelopers.net',
            'phone' => '9811657128',
            'productinfo' => 'VH products',
            'surl' => $this->getBaseURL().'/checkout/success',
            'furl' =>  $this->getBaseURL().'/checkout/failed',
            'service_provider' => 'payu_paisa',
            'address_id'=>$address_id
        );
        $hash = '';
        // Hash Sequence
        $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
        if(empty($posted['hash']) && sizeof($posted) > 0) {
          if(
                     empty($posted['key'])
                  || empty($posted['txnid'])
                  || empty($posted['amount'])
                  || empty($posted['firstname'])
                  || empty($posted['email'])
                  || empty($posted['phone'])
                  || empty($posted['productinfo'])
                  || empty($posted['surl'])
                  || empty($posted['furl'])
                  || empty($posted['service_provider'])
          ) {
            $formError = 1;
          } else {
            //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
                $hashVarsSeq = explode('|', $hashSequence);
            $hash_string = '';	
                foreach($hashVarsSeq as $hash_var) {
              $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
              $hash_string .= '|';
            }

            $hash_string .= $SALT;

            $hash = strtolower(hash('sha512', $hash_string));
            $action = $PAYU_BASE_URL . '/_payment';
          }
        } elseif(!empty($posted['hash'])) {
          $hash = $posted['hash'];
          $action = $PAYU_BASE_URL . '/_payment';
        }
          $posted['hash'] = $hash;
          $posted['action'] = $action;
//                dd($posted);
        $this->view->postedData=$posted;
//        $this->view->dataSet = $dataSet;
//        dd($dataSet); 
    }
    public function processPaymentAction(){
        $reques= $this->getRequest();
        $txid=  $this->getPost('txnid');
        
//        dd($txid);
    }

        public function confirmOrderAction(){
             
        $userId = $this->auth_user['user_id'];
        $request= $this->getRequest();
        $type=$request->getPost('payment','');
        $dataSet = $this->userCart->getCartDataByFilters(array('user_cart.user_id' => $userId));
                $this->view->dataSet = $dataSet;
        //        dd($dataSet);   
            }

    public function successAction(){

        $request= $this->getRequest();
        $userId = $this->auth_user['user_id'];
         dd($request->getPost());
        $posted = array
         (
            'user_id' => $userId,
            'transaction_id' => $request->getPost('txnid',''),
            'original_amount' => $request->getPost('amount',''),
            'total_amount' => $request->getPost('amount',''),
            'net_amount' => $request->getPost('amount',''),
            'status' => $request->getPost('status',''),
            'transaction_time' => $request->getPost('addedon',''),
            'address_id' => $request->getPost('address_id',''),
            'payuMoneyId' => $request->getPost('payuMoneyId',''),
            'discount' => $request->getPost('discount',''),
            'paypal_response' => json_encode($request->getPost())
          );
         // dd($posted);
         // $saleId = 6;
         $saleId = $this->sales->setSalesDetails($posted);

         $checkoutData = $this->userCart->getRowByFilters(array('user_cart.user_id' => $userId));
         // dd($checkoutData);
         foreach($checkoutData as $key => $value)
         {
          $check = array(
             'sales_id' =>$saleId,
             'product_id' =>$value['product_id'],
             'quantity' =>$value['quantity']
          );
         $saleProductId = $this->sales->setSalesProductDetails($check);
         // echo $saleProductId; exit();
         }

    }
    public function failedAction(){
        
        $request= $this->getRequest();
        dd($request->getPost());
//        die('failed');
    }
}
