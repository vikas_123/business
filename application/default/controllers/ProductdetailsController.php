<?php

class ProductdetailsController extends My_Controller_Action_Abstract {
     protected $products; 
     protected $category;

    public function init() {
   $this->category = new Default_Model_Category();
       
    }

    public function indexAction() {
         $products = new Default_Model_Products();
//        die('here');
        $request = $this->getRequest();
        $prod_id = $request->getParam('productid','');
        $productsData = $products->getRowByFilters(array('products.id' => $prod_id),true);
        $productsData['product_image'] = $products->getProductImages(array('product_images.product_id' => $prod_id));
        $this->view->dataSet = $productsData;
        
//        dd($productsData);
        
        $this->view->categoryList = $this->category->getRowByFilters();
        
    }
    
}



