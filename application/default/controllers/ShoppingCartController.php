<?php
//namespace Knp\Snappy;

class ShoppingCartController extends My_Controller_Action_Abstract {
 
    protected $userCart;
  

    public function init() {
        /* Initialize action controller here */
      $this->userCart = new Default_Model_UserCart();
		
    }

    public function indexAction() {
        
        $userId = $this->auth_user['user_id'];
//        dd($userId);
        $dataSet = $this->userCart->getCartDataByFilters(array('user_cart.user_id' => $userId));
        $this->view->dataSet = $dataSet;
//         dd($dataSet);
 
    }

    public function removeAction(){
        $request = $this->getRequest();
        $id = $request->getParam('id','');
        // dd($id);
        if($id) {
          $response=  $this->userCart->updateUserCartById(array('is_active' => 0,'updated_at' => time()),$id);
//          dd($response);
        }
        $this->_redirect($this->getBaseURL() . '/shopping-cart');
    }
    public function updateCartAction(){
        $request = $this->getRequest();
        $id = $request->getParam('id','');
        $quantity = $request->getPost('quantity','');
//         dd($quantity);
        if($id && $quantity) {

            $this->userCart->updateUserCartById(array('quantity' => $quantity,'updated_at' => time()),$id);
        }
        $this->_redirect($this->getBaseURL() . '/shopping-cart');
    }
    
}
