<?php

class AjaxController extends My_Controller_Action_Abstract {
     protected $products; 
     protected $userCart;
  

    public function init() {
        /* Initialize action controller here */
        $this->userCart = new Default_Model_UserCart();
        $this->products = new Default_Model_Products();
    }

    public function indexAction() {

     $request = $this->getRequest();
     dd($request->getPost());
    }
    public function addToCartAction() {

       // dd('here');
        $request = $this->getRequest();   

         if(isset($this->auth_user) && $this->auth_user['user_id']) {   
           
             $dataSet = array(
                'user_id' => $this->auth_user['user_id'],
                'product_id' => $request->getPost('productId',''),
                'created_at' => time()
                );
             
             $this->userCart->addUserCart($dataSet);
            die(json_encode(array('success' => true)));
          }   
          else {
            die(json_encode(array('success' => false)));
          }

    }

    public function updateProductSkuAction() {
          $productData = $this->products->getRowByFilters();
          $r = 10001;
          foreach($productData as $key => $product) {
               
                        $sku = 'VH-'.$r;
                     
                $productData = array('sku' => $sku);
                // dd($productData);
                $productId = $this->products->updateProductsById($productData,$product['product_id']);
                $r++;
                
          }
          die('SKU has been updated');
    }
    
    
    public function changeProductAction(){
         $senddata  = array();
        $request = $this->getRequest();
        $product_id =$request->getPost('product_id','');
        $id = $request->getPost('id','');
        
        $response=  $this->products->updateProductImages(array('is_primary'=> 0),'product_id', $product_id);
        $response1=  $this->products->updateProductImages(array('is_primary'=> 1),'id', $id);
//        dd($response);
        if($response){
            $senddata['success'] = TRUE;
        $senddata['message'] = "Success..!";
        echo json_encode($senddata);
        }
        else {
            $senddata['success'] = FALSE;
               $senddata['message'] = "Failed..!";
               echo json_encode($senddata);
        }
        
        
        die();
    
    }
    public function deleteImageAction(){ 
         $senddata  = array();
        $request = $this->getRequest();
        $product_id =$request->getPost('product_id','');
        $id = $request->getPost('id','');
        
        $response=  $this->products->deleteProductImage($id);
         if($response){
            $senddata['success'] = TRUE;
        $senddata['message'] = "Success..!";
        echo json_encode($senddata);
        }
        else {
            $senddata['success'] = FALSE;
               $senddata['message'] = "failed..!";
               echo json_encode($senddata);
        }
        
        
        die();
        
    }
    
    
    
}
