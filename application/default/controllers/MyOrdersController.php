<?php
//namespace Knp\Snappy;

class MyOrdersController extends My_Controller_Action_Abstract {
 
    protected $userCart;
    protected $sales;
    protected $salesProduct;

    public function init() {
        /* Initialize action controller here */
      $this->userCart = new Default_Model_UserCart();
      $this->sales = new Default_Model_Sales();
      $this->salesProduct = new Default_Model_SalesProduct();
    
    }

    public function indexAction() {
        
        $userId = $this->auth_user['user_id'];
//       dd($userId);
        $dataSet = $this->sales->getSalesDetails(array('sales.user_id' => $userId));
//         dd($dataSet);
        // $saleProductId = $this->sales->getSalesProductDetails(array('sales.id' => $userId));
        foreach($dataSet as $key => $value)
         {
          $dataSet[$key]['sales-product'] = $this->sales->getSalesProductDetails(array('sales_product.sales_id' => $value['id']));
        
         }
//          dd($dataSet);
        //  $saleProductId = $this->sales->setSalesProductDetails($check);
        $this->view->dataSet = $dataSet;
//         
 
    }

    // public function removeAction(){
    //     $request = $this->getRequest();
    //     $id = $request->getParam('id','');
    //     // dd($id);
    //     if($id) {
    //         $this->userCart->deleteUserCart($id);
    //     }
    //     $this->_redirect($this->getBaseURL() . '/shopping-cart');
    // }
    // public function updateCartAction(){
    //     $request = $this->getRequest();
    //     $id = $request->getParam('id','');
    //     $quantity = $request->getPost('quantity','');
    //     // dd($quantity);
    //     if($id && $quantity) {

    //         $this->userCart->updateUserCartById(array('quantity' => $quantity,'updated_at' => time()),$id);
    //     }
    //     $this->_redirect($this->getBaseURL() . '/shopping-cart');
    // }
    
}
