<?php

class ProfileController extends My_Controller_Action_Abstract {

    protected $address;
    protected $users;

    public function init() {
        $this->address = new Default_Model_Address();
        $this->users = new Default_Model_Users();
    }

    public function indexAction() {
        $request = $this->getRequest();
        $user_id = $this->auth_user['user_id'];
        $name = $request->getPost('name', '');
        if($name){
              $requestData = array(
               
                'name' => $request->getPost('name', ''),
                'phone' => $request->getPost('phone', '')
                  );
               $data = $this->users->updateUserByUserId($requestData,$user_id);
              if($data){
                   $auth = Zend_Auth::getInstance();
                   
                    $this->auth_user['name']=$request->getPost('name', '');
                    $this->auth_user['phone']=$request->getPost('phone', '');   
                    $auth->getStorage()->write($this->auth_user);
//                    dd($this->auth_user);
                    $this->view->auth_user =  $this->auth_user;
              }
               
               }
    }
    
    public function editaddressAction() {
        $request = $this->getRequest();
        $id = $request->getParam('id', '');
        $name = $request->getPost('name', '');
        $user_id = $this->auth_user['user_id'];
        $checkout = $request->getParam('checkout', '');
        if ($name) {
            $requestData = array(
                'user_id' => $user_id,
                'name' => $request->getPost('name', ''),
                'phone' => $request->getPost('phone', ''),
                'address' => $request->getPost('address', ''),
                'area' => $request->getPost('area', ''),
                'house_no' => $request->getPost('house_no', ''),
                'city' => $request->getPost('city', ''),
                'state' => $request->getPost('state', ''),
                'pincode' => $request->getPost('pincode', '')
            );
//            dd($requestData);
            if ($id) {
                $this->address->updateAddressById($requestData, $id);
            } else {
                $id = $this->address->setAddress($requestData);
            }
            if($checkout) {
                $this->_redirect($this->getBaseURL() . '/checkout');
            }
            else
            $this->_redirect($this->getBaseURL() . '/profile/myaddress');
        }

        if ($id) {
            $dataSet['addressList'] = $this->address->getRowByFilters(array('id' => $id), TRUE);
            $this->view->dataSet = $dataSet;
//            dd($dataSet);
        }
    }

    public function myaddressAction() {
        $user_id = $this->auth_user['user_id'];
        $dataSet['addressList'] = $this->address->getRowByFilters(array('user_id' => $user_id));
        $this->view->dataSet = $dataSet;
//        dd($dataSet);
    }

    public function saveaddressAction() {

    }

    public function deleteaddressAction() {
        $request = $this->getRequest();
        $id = $request->getParam('id', '');
        $checkout = $request->getParam('checkout', '');
        $user_id = $this->auth_user['user_id'];
        if($user_id){
            $response = $this->address->updateAddressById(array('is_active'=>0),$id);
        }
//        dd($response);
        if($checkout){
            $this->_redirect($this->getBaseURL() . '/checkout');
        }else
        $this->_redirect($this->getBaseURL() . '/profile/myaddress');
         
    }

}

?>
