-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 01, 2016 at 02:37 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `business`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
`id` int(11) NOT NULL,
  `category_name` varchar(100) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `subcategory` int(11) NOT NULL,
  `description` varchar(200) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `deleted_at` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`, `parent_id`, `subcategory`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Home', 0, 0, '', 1472205179, 0, 0),
(2, 'About Us', 0, 0, '', 1472205570, 0, 0),
(4, 'Services', 0, 0, '', 1472205690, 0, 0),
(5, 'Portfolio', 0, 0, '', 1472205703, 0, 0),
(6, 'Pages', 0, 0, '', 1472205728, 0, 0),
(7, 'Blog', 0, 0, '', 1472205734, 0, 0),
(10, 'Contact Us', 0, 0, '', 1472206614, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
`id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` int(11) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `price` int(11) NOT NULL,
  `message` varchar(200) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `deleted_at` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `phone`, `subject`, `price`, `message`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'priyanka singh', 'p@gmail.com', 88888888, 'check', 0, 'check message', 1472756106, 0, 0),
(2, 'akash', 'akash@gmail.com', 22222222, 'subject', 0, 'just check..', 1472756334, 0, 0),
(12, 'atul', 'atul@gmail.com', 66666666, 'subject', 30000, 'no message...', 1472719385, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `portfolio`
--

CREATE TABLE IF NOT EXISTS `portfolio` (
`id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `image` varchar(500) NOT NULL,
  `description` varchar(600) NOT NULL,
  `type` varchar(30) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `deleted_at` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `portfolio`
--

INSERT INTO `portfolio` (`id`, `name`, `title`, `image`, `description`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'portfolio1', 'There are many variations of passages of Lorem Ipsum available, but the majority', 'post_image_1472599377.png', '<p>There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority</p><p><br></p><p>There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority<br></p>', 'default', 1472599377, 0, 0),
(2, 'portfolio2', 'There are many variations of passages of Lorem Ipsum available, but the majority', 'post_image_1472599436.png', '<p>There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority</p><p><br></p><p>There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority<br></p>', 'creative', 1472599436, 0, 0),
(3, 'portfolio3', 'There are many variations of passages of Lorem Ipsum available, but the majority', 'post_image_1472599454.png', '<p>There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority</p><p><br></p><p>There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority<br></p>', 'webdevelopment', 1472599454, 0, 0),
(4, 'portfolio4', 'There are many variations of passages of Lorem Ipsum available, but the majority', 'post_image_1472599482.png', '<p>There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority</p><p><br></p><p>There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority<br></p>', 'creative', 1472599482, 0, 0),
(5, 'portfolio5', 'There are many variations of passages of Lorem Ipsum available, but the majority', 'post_image_1472599497.png', '<p>There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority</p><p><br></p><p>There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority<br></p>', 'default', 1472599497, 0, 0),
(9, 'portfolio6', 'There are many variations of passages of Lorem Ipsum available, but the majority', 'post_image_1472601145.png', '<p>There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority</p><p><br></p><p>There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority.There are many variations of passages of Lorem Ipsum available, but the majority<br></p>', 'photography', 1472601145, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio`
--
ALTER TABLE `portfolio`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `portfolio`
--
ALTER TABLE `portfolio`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
